package com.ajh.webshell.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import com.ajh.webshell.message.ShellActionMessage;
import com.ajh.webshell.message.ShellActionMessage.ActionType;
import com.ajh.webshell.thread.ShellMainThread;
import com.ajh.webshell.thread.ShellStdInThread;
import com.ajh.webshell.thread.ShellStdOutThread;
import com.ajh.webshell.thread.ShellThread;
import com.ajh.webshell.thread.ShellThreadCollection;

@Service
public class AsyncShellService {
	private static final Logger logger = LoggerFactory.getLogger(AsyncShellService.class);

	@Autowired
	private ApplicationContext appCtxt;

	@Autowired
	@Qualifier(value="threadPoolTaskExecutor")
	private TaskExecutor taskExecutor;

	private Map<String, ShellThreadCollection> threads = new HashMap<String, ShellThreadCollection>();

	private void removeThreadCollection(String simpSessionId) {
		logger.info("All threads exited, remove record from map");
		threads.remove(simpSessionId);
	}

	/*
	 *  1) SIGHUP       2) SIGINT       3) SIGQUIT      4) SIGILL       5) SIGTRAP
	 *  6) SIGABRT      7) SIGBUS       8) SIGFPE       9) SIGKILL     10) SIGUSR1
	 * 11) SIGSEGV     12) SIGUSR2     13) SIGPIPE     14) SIGALRM     15) SIGTERM
	 * 16) SIGSTKFLT   17) SIGCHLD     18) SIGCONT     19) SIGSTOP     20) SIGTSTP
	 * 21) SIGTTIN     22) SIGTTOU     23) SIGURG      24) SIGXCPU     25) SIGXFSZ
	 * 26) SIGVTALRM   27) SIGPROF     28) SIGWINCH    29) SIGIO       30) SIGPWR
	 * 31) SIGSYS      34) SIGRTMIN    35) SIGRTMIN+1  36) SIGRTMIN+2  37) SIGRTMIN+3
	 * 38) SIGRTMIN+4  39) SIGRTMIN+5  40) SIGRTMIN+6  41) SIGRTMIN+7  42) SIGRTMIN+8
	 * 43) SIGRTMIN+9  44) SIGRTMIN+10 45) SIGRTMIN+11 46) SIGRTMIN+12 47) SIGRTMIN+13
	 * 48) SIGRTMIN+14 49) SIGRTMIN+15 50) SIGRTMAX-14 51) SIGRTMAX-13 52) SIGRTMAX-12
	 * 53) SIGRTMAX-11 54) SIGRTMAX-10 55) SIGRTMAX-9  56) SIGRTMAX-8  57) SIGRTMAX-7
	 * 58) SIGRTMAX-6  59) SIGRTMAX-5  60) SIGRTMAX-4  61) SIGRTMAX-3  62) SIGRTMAX-2
	 * 63) SIGRTMAX-1  64) SIGRTMAX
	 *
	 * */
	private void sendSysSignalToProcess(String signal, long pid) {
		logger.info("Send signal {} to process({})", signal.toUpperCase(), pid);
		// Should send signal to child process not shell process
		try {
			ProcessBuilder pb = new ProcessBuilder("kill", "-s", signal.toUpperCase(), String.valueOf(pid));
			pb.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void execAsync(Object notifier) {
		if (notifier instanceof GenericMessage<?>) {
			@SuppressWarnings("unchecked")
			ShellActionMessage sam = ((GenericMessage<ShellActionMessage>)notifier).getPayload();
			String simpSessionId = sam.getSimpSessionId();
			String userSessionId = sam.getUserSessionId();

			ShellThreadCollection stc = (ShellThreadCollection)threads.get(simpSessionId);
			if (stc == null) {
				if (sam.getActionType() == ActionType.START_MAIN) {
					stc = new ShellThreadCollection();
					threads.put(simpSessionId, stc); // When to remove?
				} else {
					// Report error
					logger.error("Not allowed to do action {}, simp-session-id={}, user-session-id={}", sam, simpSessionId, userSessionId);
					return;
				}
			}

			switch (sam.getActionType()) {
			case START_MAIN:
				if (stc.getMain() == null) {
					ShellThread main = appCtxt.getBean(ShellMainThread.class);
					main.setName("shell main");
					main.setSimpSessionId(simpSessionId);
					main.setUserSessionId(userSessionId);
					stc.setMain(main);
					taskExecutor.execute(main);
				}
				break;
			case START_STDOUT:
				if (stc.getStdout() == null) {
					ShellThread stdout = appCtxt.getBean(ShellStdOutThread.class);
					stdout.setName("shell stdout");
					stdout.setSimpSessionId(simpSessionId);
					stdout.setUserSessionId(userSessionId);
					stdout.setStreamInputOrOutput((InputStream)sam.getPayload());
					stc.setStdout(stdout);
					taskExecutor.execute(stdout);
				}
				break;
			case START_STDIN:
				if (stc.getStdin() == null) {
					ShellThread stdin = appCtxt.getBean(ShellStdInThread.class);
					stdin.setName("shell stdin");
					stdin.setSimpSessionId(simpSessionId);
					stdin.setStreamInputOrOutput(sam.getPayload());
					stc.setStdin(stdin);
//					taskExecutor.execute(stdin); // Not necessary making it running
				}
				break;
			case STOP_STDOUT:
				if (stc.getStdout() != null) {
					stc.getStdout().setExitCmdReceived(true);
					if (stc.getStdin().isExitCmdReceived()) {
						// All threads exited, remove record from map
						removeThreadCollection(simpSessionId);
					}
				}
				break;
			case STOP_STDIN:
				if (stc.getStdin() != null) {
					stc.getStdin().setExitCmdReceived(true);
					if (stc.getStdout().isExitCmdReceived()) {
						// All threads exited, remove record from map
						removeThreadCollection(simpSessionId);
					}
				}
				break;
			case EXEC_CMD:
//				logger.info("RUN " + san.getPayload());
				if (stc.getStdin() != null && !stc.getStdin().isExitCmdReceived())
					stc.getStdin().execOnce((String)sam.getPayload());
				break;
			case SEND_SIG:
				sendSysSignalToProcess((String)sam.getPayload(), stc.getMain().getPid());
				break;
			default:
				break;
			}
		} else {
			logger.error("Message object is not instance of GenericMessage<>!");
		}
	}
}
