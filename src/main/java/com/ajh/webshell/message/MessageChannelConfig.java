package com.ajh.webshell.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;

import com.ajh.webshell.async.AsyncShellService;

@Configuration
@EnableIntegration
public class MessageChannelConfig {
	@Autowired
	private AsyncShellService shell;

	@Bean
	/*
	 * DirectChannel works within a thread, use ExecutorChannel instead.
	 * */
	public ExecutorChannel shellActionMessageChannel() {
		return MessageChannels.executor("shellActionMessageChannel", new SimpleAsyncTaskExecutor()).get();
	}

	@Bean
	public IntegrationFlow simpleFlow() {
		return IntegrationFlows
				.from(shellActionMessageChannel())
				.filter(ShellActionMessage.class, ShellActionMessage::isAccepted)
				.handle(shell::execAsync)
				.get();
	}
}
