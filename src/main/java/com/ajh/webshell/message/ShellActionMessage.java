package com.ajh.webshell.message;

import java.util.Objects;

public class ShellActionMessage {
	public static enum ActionType {
		START_MAIN, SET_MAIN_PID,
		START_STDOUT, START_STDERR, START_STDIN,
		STOP_STDOUT, STOP_STDERR, STOP_STDIN,
		EXEC_CMD, SEND_SIG
	}

	private String simpSessionId;
	private String userSessionId;

	private ActionType actionType;
	private Object payload; // InputStream for stdout
	                        // OutputStream for stdin
	                        // String of command for stdin
	                        // String of signal for shell process

	public ShellActionMessage(String simpSessionId, String userSessionId, ActionType actionType) {
		this(simpSessionId, userSessionId, actionType, null);
	}

	public ShellActionMessage(String simpSessionId, String userSessionId, ActionType actionType, Object payload) {
		this.simpSessionId = simpSessionId;
		this.userSessionId = userSessionId;
		this.actionType = actionType;
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "{'actionType'='" + actionType.name() + "'}";
	}

	@Override
	public int hashCode() {
		return Objects.hash(actionType, payload);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ShellActionMessage)) {
			return false;
		}
		ShellActionMessage other = (ShellActionMessage) obj;
		return actionType == other.actionType && Objects.equals(payload, other.payload);
	}

	public ActionType getActionType() {
		return actionType;
	}

	public Object getPayload() {
		return payload;
	}

	public boolean isAccepted() {
		return true;
	}

	public String getSimpSessionId() {
		return simpSessionId;
	}

	public String getUserSessionId() {
		return userSessionId;
	}

	public void setSimpSessionId(String simpSessionId) {
		this.simpSessionId = simpSessionId;
	}

	public void setUserSessionId(String userSessionId) {
		this.userSessionId = userSessionId;
	}
}
