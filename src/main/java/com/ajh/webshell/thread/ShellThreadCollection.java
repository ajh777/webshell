package com.ajh.webshell.thread;

public class ShellThreadCollection {
	private ShellThread main = null;
	private ShellThread stdin = null;
	private ShellThread stdout = null;

	public ShellThreadCollection() {
	}

	public ShellThreadCollection(ShellThread main, ShellThread stdin, ShellThread stdout) {
		this.main = main;
		this.stdin = stdin;
		this.stdout = stdout;
	}

	public ShellThread getMain() {
		return main;
	}

	public ShellThread getStdin() {
		return stdin;
	}

	public ShellThread getStdout() {
		return stdout;
	}

	public void setMain(ShellThread main) {
		this.main = main;
	}

	public void setStdin(ShellThread stdin) {
		this.stdin = stdin;
	}

	public void setStdout(ShellThread stdout) {
		this.stdout = stdout;
	}

}
