package com.ajh.webshell.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.ajh.webshell.message.ShellActionMessage;
import com.ajh.webshell.message.ShellActionMessage.ActionType;

@Component
@Scope("prototype")
public class ShellMainThread extends ShellThread {
	private static final Logger logger = LoggerFactory.getLogger(ShellMainThread.class);

	@Autowired
	@Qualifier(value="shellActionMessageChannel")
	private MessageChannel msgChannel = null;

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@Override
	public void exec() {
//		logger.info("Run shell thread...");
		try {
//			ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", "env");
			ProcessBuilder pb = new ProcessBuilder("/bin/bash");
			pb.redirectErrorStream(true); // Redirect stderr to stdout.

			final Process p = pb.start();
			this.setPid(p.pid()); // JDK 9 and onward

			logger.info("Returning shell thread IO stream...");

			if (msgChannel != null) {
				logger.info("Sending message to AsyncShellService...");
				msgChannel.send(
						MessageBuilder
							.withPayload(new ShellActionMessage(getSimpSessionId(), getUserSessionId(), ActionType.START_STDOUT, p.getInputStream()))
							.build());
				msgChannel.send(new GenericMessage<>(new ShellActionMessage(getSimpSessionId(), getUserSessionId(), ActionType.START_STDIN, p.getOutputStream())));
			} else {
				logger.error("Null message channel!");
			}

			int ret = p.waitFor();
			logger.info("Exit shell process, code=" + ret);

			// Terminate stdin & stdout threads
			msgChannel.send(
					MessageBuilder
						.withPayload(new ShellActionMessage(getSimpSessionId(), getUserSessionId(), ActionType.STOP_STDOUT))
						.build());
			msgChannel.send(
					MessageBuilder
						.withPayload(new ShellActionMessage(getSimpSessionId(), getUserSessionId(), ActionType.STOP_STDIN))
						.build());

			this.simpMessagingTemplate.convertAndSendToUser(getUserSessionId(), "/out-of-service", "out of service"); // /unicast/{userid}/out-of-service

			setExitCmdReceived(true); // Terminate current thread
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}

}
