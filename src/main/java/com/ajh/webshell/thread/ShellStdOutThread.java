package com.ajh.webshell.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

@Component
@Scope("prototype")
public class ShellStdOutThread extends ShellThread {
	private static final Logger logger = LoggerFactory.getLogger(ShellStdOutThread.class);

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

//	@Autowired
//	private WebSocketProperties wsProps;

	private BufferedReader stdout = null;

	public ShellStdOutThread() {
		
	}

	@Override
	protected void cleanup() {
		if (stdout != null) {
			try {
				stdout.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void exec() {
//		logger.info("Run stdout stream thread...");

		if (this.getStreamInputOrOutput() == null) {
			logger.warn("Null input stream, exit immediately."); 
		}

		if (stdout == null) {
			stdout = new BufferedReader(new InputStreamReader((InputStream)this.getStreamInputOrOutput()));
		}

		try {
			String line;
			while ((line = stdout.readLine()) != null) { // Read till end-of-file
//				logger.info("ws({}) >> {}", getUserSessionId(), line);
				line = HtmlUtils.htmlEscape(line);
				line = line.replaceAll(" ", "&nbsp;");
				line = line.replaceAll("\t", "&nbsp;&nbsp;");
//				this.simpMessagingTemplate.convertAndSend(wsProps.getBrokerAnycast()+"/stdout", line);
				this.simpMessagingTemplate.convertAndSendToUser(getUserSessionId(), "/stdout", line); // /unicast/{userid}/stdout
			}
			Thread.sleep(1);
		} catch (Exception e) {
			e.printStackTrace();
//			logger.error(ex.getMessage());
		}

//		logger.info("Exit stdout stream thread...");
	}

}
