package com.ajh.webshell.thread;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ShellStdInThread extends ShellThread {
	private static final Logger logger = LoggerFactory.getLogger(ShellStdInThread.class);

	private BufferedWriter stdin = null;

	@Override
	protected void cleanup() {
		if (stdin != null) {
			try {
				stdin.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void execOnce(String cmd) {
		logger.info("RUN " + cmd);

		if (stdin == null) {
			stdin = new BufferedWriter(new OutputStreamWriter((OutputStream)this.getStreamInputOrOutput()));
		}

		try {
			stdin.write(cmd+'\n'); // Write to stdin of executed process
			stdin.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
