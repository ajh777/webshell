package com.ajh.webshell.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ShellThread implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(ShellThread.class);

	private String name = "unamed";
	private Object streamInputOrOutput = null;
	private boolean isExitCmdReceived = false;

	private String simpSessionId = null;
	private String userSessionId = null;

	private long pid = -1;

	public ShellThread() {
	}

	public ShellThread(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getStreamInputOrOutput() {
		return streamInputOrOutput;
	}

	public void setStreamInputOrOutput(Object streamInputOrOutput) {
		this.streamInputOrOutput = streamInputOrOutput;
	}

	public boolean isExitCmdReceived() {
		return isExitCmdReceived;
	}

	public void setExitCmdReceived(boolean isExitCmdReceived) {
		this.isExitCmdReceived = isExitCmdReceived;
	}

	public String getSimpSessionId() {
		return simpSessionId;
	}

	public void setSimpSessionId(String simpSessionId) {
		this.simpSessionId = simpSessionId;
	}

	public String getUserSessionId() {
		return userSessionId;
	}

	public void setUserSessionId(String userSessionId) {
		this.userSessionId = userSessionId;
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	@Override
	final public void run() {
		logger.info("Run " + name + " thread...");

		while (!this.isExitCmdReceived) {
			exec(); // Run business
			try {
				Thread.sleep(1); // Proper setting?
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		cleanup();
		logger.info("Exit " + name + " thread...");
	}

	protected void cleanup() {
	}

	// Run business
	protected void exec() {
	}

	public void execOnce(String cmd) {
	}
}
