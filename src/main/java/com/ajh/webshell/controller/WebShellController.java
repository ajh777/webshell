package com.ajh.webshell.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ajh.webshell.message.ShellActionMessage;
import com.ajh.webshell.message.ShellActionMessage.ActionType;

@Controller
public class WebShellController {
	private static final Logger logger = LoggerFactory.getLogger(WebShellController.class);

	@Autowired
	@Qualifier(value="shellActionMessageChannel")
	private MessageChannel msgChannel;

	@GetMapping("/")
	public String startShellThread(Model model) {
		return "index";
	}

	@MessageMapping("/start") // /app/start
	public void startWebShell(String message, StompHeaderAccessor stompHeader, @Header("user-session-id") String userSessionId, @Payload String body) {
		logger.info("ws << " + message);
		logger.info("ws << simp-session-id=" + stompHeader.getSessionId());
		logger.info("ws << user-session-id=" + userSessionId);
		msgChannel.send(
			MessageBuilder
				.withPayload(new ShellActionMessage(stompHeader.getSessionId(), userSessionId, ActionType.START_MAIN, null))
				.build());
	}

	@MessageMapping("/command") // /app/command
	public void runCommand(String message, StompHeaderAccessor stompHeader, @Header("user-session-id") String userSessionId, @Payload String command) {
		logger.info("ws << " + command);
		logger.info("ws << simp-session-id=" + stompHeader.getSessionId());
		logger.info("ws << user-session-id=" + userSessionId);
		msgChannel.send(
				MessageBuilder
					.withPayload(new ShellActionMessage(stompHeader.getSessionId(), userSessionId, ActionType.EXEC_CMD, command))
					.build());
	}

	@MessageMapping("/signal") // /app/signal
	public void sendSignal(String message, StompHeaderAccessor stompHeader, @Header("user-session-id") String userSessionId, @Payload String signal) {
		logger.info("ws << " + signal);
		logger.info("ws << simp-session-id=" + stompHeader.getSessionId());
		logger.info("ws << user-session-id=" + userSessionId);
		msgChannel.send(
				MessageBuilder
					.withPayload(new ShellActionMessage(stompHeader.getSessionId(), userSessionId, ActionType.SEND_SIG, signal))
					.build());
	}
}
