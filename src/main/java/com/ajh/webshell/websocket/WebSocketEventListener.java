package com.ajh.webshell.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.ajh.webshell.message.ShellActionMessage;
import com.ajh.webshell.message.ShellActionMessage.ActionType;

@Component
public class WebSocketEventListener {
	private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

	@Autowired
	@Qualifier(value="shellActionMessageChannel")
	private MessageChannel msgChannel;

	@EventListener
	public void handleConnecListener(SessionConnectedEvent event) {
		logger.info("ws connect: {}", event.getMessage());

		StompHeaderAccessor headers = StompHeaderAccessor.wrap(event.getMessage());
		logger.info("ws sessionid="+headers.getSessionId());
	}

	@EventListener
	public void handleDisconnecListener(SessionDisconnectEvent event) {
		logger.info("ws disconnect: {}", event.getMessage());

		StompHeaderAccessor headers = StompHeaderAccessor.wrap(event.getMessage());
		logger.info("ws sessionid="+headers.getSessionId());

		msgChannel.send(
			MessageBuilder
				.withPayload(new ShellActionMessage(headers.getSessionId(), null, ActionType.EXEC_CMD, "exit"))
				.build());
	}
}
