package com.ajh.webshell.websocket;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="webshell.ws")
public class WebSocketProperties {
	private String app = "/app";
	private String brokerAnycast = "/anycast";
	private String brokerUnicast = "/unicast";
	private String endpoint = "/stomp";

	public String getApp() {
		return app;
	}

	public String getBrokerAnycast() {
		return brokerAnycast;
	}

	public String getBrokerUnicast() {
		return brokerUnicast;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public void setBrokerAnycast(String brokerAnycast) {
		this.brokerAnycast = brokerAnycast;
	}

	public void setBrokerUnicast(String brokerUnicast) {
		this.brokerUnicast = brokerUnicast;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}


}
