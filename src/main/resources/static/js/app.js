// See example at https://spring.io/guides/gs/messaging-stomp-websocket/

var stompClient = null;
var userSessionId = uuid.v1();

function connect() {
//var stompClient = null;
  var socket = new SockJS('/stomp');
//var userSessionId = $("#_csrf").val(); // Use CSRF token as user session id but fail due to same token on multiple pages
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function (frame) { // How to get session id???
    console.log('Stomp connected: ' + frame);
    // Register anycast message handler
    stompClient.subscribe('/anycast/stdout', function (data) {
//    console.log('>>>>> ' + data.body);
//    var json = JSON.parse(data.body);
      var result = "<span class='mono'>" + data.body + "</span><br/>";
      appendMoreOutput(result);
    });
    // Register unicast message handler
    stompClient.subscribe('/unicast/' + userSessionId + '/stdout', function (data) {
      var result = "<span class='mono'>" + data.body + "</span><br/>";
      appendMoreOutput(result);
    });
    
    stompClient.subscribe('/unicast/' + userSessionId + '/out-of-service', function (data) {
        $("#cmdtxt").prop('disabled', true); // Disabled for input
      });

    headers= {"user-session-id": userSessionId};
    stompClient.send("/app/start", headers, "start trigger message");
  });
}

function disconnect() {
  if (stompClient !== null) {
    stompClient.disconnect();
  }
  console.log("Disconnected")
}

function registerCommandSubmission() {
  $("#cmdtxt").on("keypress", function (e) {
    if (e.keyCode === 13) { // Enter key
      var cmdstr = $(this).val();
      console.log("EXEC > " + cmdstr);
      headers= {"user-session-id": userSessionId};
      stompClient.send("/app/command", headers, cmdstr);
      appendMoreOutput("<span class='mono'>$ " + cmdstr + "</span><br/>");
      $(this).val("");
    }
  });
}

function appendMoreOutput(content) {
  $("#output").append(content);
  scrollOutputDivToEnd();
}

function scrollOutputDivToEnd() {
  $("#output").scrollTop(function() { return this.scrollHeight; });
}

function registerHotkeyCtrlPlusC() {
  // Capture ctrl+c
  var cKey = 67;
  $(document).keydown(function(event) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode === cKey) {
      console.log("Got ctrl+c");
      headers= {"user-session-id": userSessionId};
      stompClient.send("/app/signal", headers, "sigint");
      event.preventDefault();
      return false;
    }
  });
}

// Entry point
// Shorthand for $(document).ready()
$(function() {
  registerHotkeyCtrlPlusC();
  connect();
  if (stompClient !== null) {
    registerCommandSubmission();
  }
});
